from mtbalance.api import Quantos


def connect(self):
    pass


def _response_monitor(self):
    while True:
        pass


# override connection and listener methods
Quantos.connect = connect
Quantos._response_monitor = _response_monitor

qt_debug = Quantos(
    '10.0.0.2:8014',
    logging_level=10,
)
