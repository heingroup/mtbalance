import unittest

from unithandler import UnitFloat
from random import random, uniform
from mtbalance.commands import QuantosCommand, QuantosResponse, BalanceResponse, BalanceCommand
from mtbalance.commands.balance import SUPPORTED_COMMANDS
from mtbalance.commands.quantos import COMMAND_MAP


class TestBalanceCommandMap(unittest.TestCase):
    def test_balance_commands(self):
        for command_key in SUPPORTED_COMMANDS:
            cmd = BalanceCommand(command_key)
            self.assertEqual(cmd.name, SUPPORTED_COMMANDS[command_key]['name'])


class TestQuantosCommandMap(unittest.TestCase):
    def check_command(self, dct, *inds):
        """checks an individual command"""
        cmd = f'QRD {" ".join([str(ind) for ind in inds])}'
        command = QuantosCommand(cmd)
        if command._indicies == [60, 7]:
            print('here')
        self.assertEqual(cmd, command.command)
        self.assertEqual(dct['name'], command.name)
        if 'A' in dct:
            if any([type(key) is int for key in dct.keys()]):
                for num_key in dct.keys() & set(range(10)):
                    resp = QuantosResponse(f'{cmd} {num_key} A')
                    # check that response code is retrieved
                    self.assertEqual(resp.response_code, num_key)
                    self.assertEqual(resp.response, dct[num_key])
            else:
                resp = QuantosResponse(f'{cmd} A')
                self.assertEqual(resp.response_code, 'A')
                self.assertEqual(resp.response, dct['A'])
        if 'B' in dct:
            resp = QuantosResponse(f'{cmd} B')
            self.assertEqual(resp.response, dct['B'])

        # check parameter error catch
        parse_error = f'QRD {" ".join([str(ind) for ind in inds[:-1]])} L'
        parse_error_resp = QuantosResponse(parse_error)
        self.assertIsNotNone(parse_error_resp.error)
        self.assertEqual(parse_error_resp.error_code, -1)

    def test_quantos_commands(self):
        """tests all command combinations"""
        for t1, t1_dct in COMMAND_MAP.items():
            if 'name' in t1_dct:
                self.check_command(t1_dct, t1)
                continue
            for t2, t2_dct in t1_dct.items():
                if 'name' in t2_dct:
                    self.check_command(t2_dct, t1, t2)
                    continue
                for t3, t3_dct in t2_dct.items():
                    self.check_command(t3_dct, t1, t2, t3)

    def test_dosing_targets(self):
        """tests dosing target values"""
        for _ in range(1000):
            val = round(
                uniform(10.0, 250000.00),
                2
            )
            cmd = QuantosCommand(f'QRD 1 1 5 {val}')
            self.assertEqual(cmd.value, str(val))


class TestResponses(unittest.TestCase):
    """tests response classes"""
    def test_quantos_responses(self):
        error_response = 'QRD 2 3 7 I 2\r\n'
        qr_err = QuantosResponse(error_response)
        self.assertIsNotNone(qr_err.error)
        self.assertEqual(qr_err.error, 'Another job is running.')
        self.assertEqual(qr_err.error_code, 2)
        self.assertEqual(qr_err.name, 'enquiry of the front door position')

        answer_response = 'QRD 2 3 7 3 A\r\n'
        qr_ans = QuantosResponse(answer_response)
        self.assertIsNone(qr_ans.error)
        self.assertEqual(qr_ans.response_interpretation, 'front door position open')
        self.assertEqual(qr_ans.response_code, 3)
        self.assertEqual(qr_ans.response, 'open')
        self.assertEqual(qr_ans.command, 'QRD 2 3 7')

        bad_parameter = 'QRD 2 3 L\r\n'
        qr_bad = QuantosResponse(bad_parameter)
        self.assertIsNotNone(qr_bad.error)
        self.assertEqual(qr_bad.error_code, -1)
        self.assertEqual(qr_bad.error, 'Command understood but was not executable due to an incorrect parameter')

        in_process = 'QRD 2 4 11 B\r\n'
        qr_ip = QuantosResponse(in_process)
        self.assertEqual(qr_ip.response_code, 'B')
        self.assertEqual(qr_ip.response_interpretation, ' Command understood, sending XML')

        # test command linkages
        command = QuantosCommand('QRD 2 3 7\r\n')
        self.assertTrue(
            qr_err.is_response_to_command(command)
        )

    def test_special(self):
        """tests the special case where a value is included with a command"""
        parsed = QuantosCommand('QRA 60 7 3')
        self.assertEqual(parsed.value, '3')
        self.assertEqual(parsed.command_with_value, 'QRA 60 7 3')

        in_prog = QuantosResponse('QRA 60 7 B')
        self.assertEqual(in_prog.response, 'Executing command')
        done = QuantosResponse('QRA 60 7 A')
        self.assertEqual(done.response_code, 'A')
        self.assertEqual(done.response, 'Command executed and complete')

    def test_balance_responses(self):
        stable_response = 'S S      123.45 g'
        parsed_sr = BalanceResponse(stable_response)
        self.assertEqual(parsed_sr.value, '123.45 g')
        self.assertIsNone(parsed_sr.error)
        self.assertEqual(parsed_sr.parsed_value.unit, 'g')

        # test error catch
        overweight_response = BalanceResponse('S +')
        self.assertIsNotNone(overweight_response.error)
        self.assertEqual(overweight_response.error, 'balance in overload range')

        # check general code responses
        for code in ['I', 'L', '+', '-']:
            parsed = BalanceResponse(f'S {code}')
            self.assertIsNotNone(parsed.error)
            self.assertEqual(parsed.error_code, code)

    def test_balance_values(self):
        """tests value parsing of balance response"""
        non_error_keys = SUPPORTED_COMMANDS.keys() - {'ES', 'EL', 'ET'}
        for key in non_error_keys:
            commands = SUPPORTED_COMMANDS[key]
            keys = set(commands.keys()) - {'name'}
            for char in keys:
                val = random()
                str_val = f'{val} g'
                parsed = BalanceResponse(f'{key} {char} {str_val}')
                self.assertEqual(parsed.value, str_val)
                self.assertEqual(parsed.parsed_value, UnitFloat(str_val))
                if char not in ['S', 'A', 'D']:
                    self.assertIsNotNone(parsed.error)
                    self.assertEqual(parsed.error, commands[char])
