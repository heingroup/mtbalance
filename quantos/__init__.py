"""
Package for interacting with a Mettler Toledo quantos.

"""
import warnings
from mtbalance import Quantos, Balance, AugmentedQuantos

warnings.warn(  # v1.1.0
    'the quantos package has been renamed to mtbalance, please update your imports',
    DeprecationWarning,
    stacklevel=2,
)

__all__ = [
    'Balance',
    'Quantos',
    'AugmentedQuantos',
]
