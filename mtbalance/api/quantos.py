import logging
import threading
from typing import Union

from ..commands import QuantosCommand, QuantosResponse, BalanceCommand, BalanceResponse
from ..data import DispensingHead, SampleData
from .balance import Balance


logger = logging.getLogger(__name__)


class Quantos(Balance):
    DEFAULT_PORT = 8014
    _tolerance_modes = ['+/-', '0/+']  # valid tolerance modes
    _algorithms = ['standard', 'advanced']  # valid powder dosing algorithms

    # flag which defines behaviour when an action is called outside of a method
    # e.g. setting front_door_position = 'open' will wait for the Quantos to open before returning
    wait_by_default: bool = True

    WAIT_AFTER_CANCEL = 12.  # qualitatively established by watching the "home" time of a quantos with the door open

    def __init__(self,
                 address: str,
                 tcp_port: int = 8014,
                 logging_level: int = logging.INFO,
                 # todo should we set default values on instantiation?
                 ):
        """
        Object for API-control of a Quantos

        :param address: IP address of the instrument
        :param tcp_port: port (default 8014)
        :param logging_level: logging level for the logger used (passed through to logging)
        """
        self._last_xml_response = ''  # last XML response
        self._capture_flag = False  # capture flag for combining responses
        self._last_response: Union[QuantosResponse, BalanceResponse] = None  # last response
        self._last_command: Union[QuantosCommand, BalanceCommand] = None  # last command

        # parameter initial states
        self._tap_before_dosing = None
        self._tap_while_dosing = None
        self._tapper_intensity = None
        self._tapper_duration = None
        self._target_mass = None
        self._tolerance_percent = None
        self._tolerance_mode = None
        self._sample_id = None
        self._user_id = None
        self._algorithm = None
        self._activate_antistatic = None

        Balance.__init__(
            self,
            address=address,
            tcp_port=tcp_port,
            logging_level=logging_level,
        )

    def __repr__(self):
        return f'{self.__class__.__name__}({self.address}:{self.port})'

    def __str__(self):
        return f'{self.__class__.__name__} at address {self.address}:{self.port}'

    @staticmethod
    def _string_command_parse(command_string: str) -> Union[BalanceCommand, QuantosCommand]:
        """
        Parses a command string into either a BalanceCommand or QuantosCommand

        :param command_string: command string to parse
        :return: command class
        """
        for cls in [QuantosCommand, BalanceCommand]:
            try:  # try to instantiate each class in order
                return cls(command_string)
            except ValueError as e:
                err = e
                continue
        raise ValueError(err)  # if none could be instantiated, raise

    @staticmethod
    def _string_response_parse(response_string: str) -> Union[QuantosResponse, BalanceResponse]:
        """
        Parses a string from a quantos response into either a BalanceResponse or QuantosResponse

        :param response_string: response string to parse
        :return: response class
        """
        # todo see if there's a better way to do this
        for cls in [QuantosResponse, BalanceResponse]:
            try:  # try to instantiate each class in order
                return cls(response_string)
            except ValueError as e:
                err = e
                continue
        raise ValueError(err)  # if none could be instantiated, raise

    @property
    def busy(self) -> bool:
        """whether the API is aware of the Quantos being busy"""
        # todo figure out a better way to set this
        #  - set the value if there is a wait associated with a command?
        return self._sr_lock.locked()

    @property
    def last_command(self) -> Union[BalanceCommand, QuantosCommand]:
        """last command sent to the quantos"""
        return self._last_command

    @property
    def last_response(self) -> Union[BalanceResponse, QuantosResponse]:
        """last response from the Quantos"""
        return self._last_response

    @property
    def last_xml_response(self) -> str:
        """last XML response retruned by the Quantos"""
        if self._sr_lock.locked() is True:  # lock retrieval of the last response until the lock is released
            self.logger.info('The Quantos is returning a response, waiting for the response to complete.')
            while self._sr_lock.locked() is True:
                pass
        return self._last_xml_response

    def _handle_response(self, response_string: str):
        """
        Parses a Quantos response and performs the appropriate actions corresponding to that response.

        :param response_string: quantos response object to interpret
        """
        try:  # attempt to parse as a command/response
            super(Quantos, self)._handle_response(response_string)
        except ValueError as e:
            if self._capture_flag is True:  # if capture flag is enabled, add to response
                self._last_xml_response += response_string
                return
            else:
                logger.error(f'the returned response string could not be interpreted: "{response_string}"')
                raise ValueError(e)

        response = self._last_response
        # if there was an answer
        if response.response is not None:
            self.logger.debug(response)
            if response.response_code == 'B':  # special "in progress" code
                # if response indicates pending XML return, reset capture object and set flag
                if response.command in [
                    'QRD 2 4 11',  # head data
                    'QRD 2 4 12',  # sample data
                ]:
                    self._last_xml_response = ''
                    self._capture_flag = True  # more info to come, set capture flag

            else:  # otherwise is an answer response
                # if in capture mode, disable flag and don't overwrite last response
                if self._capture_flag is True:
                    self._capture_flag = False

    def _warn_not_set(self, key: str, name: str):
        """
        Logs a warning if the key has not been set in the instance.

        :param key: key defined in the dictionary
        :param name: name which the user interacted with
        """
        # todo come up with a better way to handle this
        if self.__getattribute__(key) is None:
            self.logger.error(f'{name} has not been set')

    def _send_command(self, cmd: Union[str, BalanceCommand, QuantosCommand]):
        """
        Sends the command to the Quantos. Command structure is expected to conform to the Commands and Responses format.
        This method should not be used on its own since it sends and does not wait for a response. Users should interact
        with send_and_retrieve_response unless they have a specific reason to not wait for a response.

        :param cmd: correctly formatted command
        """
        if self._sr_lock.locked():
            self.logger.info('send/receive is locked while a response is retrieve, waiting...')
            while self._sr_lock.locked() is True:  # wait for lock to clear
                pass
        super(Quantos, self)._send_command(cmd)

    def send_and_wait_for_completion(self, cmd: Union[str, QuantosCommand]):
        """
        Sends the command and waits for command completion. This method is useful for commands where
        an action is requested and it takes some time to complete the command.

        :param cmd: string command
        """
        # todo consider (if possible) consolidating the three command methods into one
        #  with kwarg modification of behaviour
        # parse the command
        if type(cmd) is not QuantosCommand:
            parsed_command = QuantosCommand(cmd)
        else:
            parsed_command = cmd
        if 'B' not in parsed_command.dct:
            # if there is no wait associated with the command, simply send it and return
            return self._send_command(cmd)

        # send the command (clearing the last response)
        self.retrieve_response(cmd)
        while self.last_response.response_code == 'B':
            if self.last_response.error is not None:
                raise ValueError(self.last_response)
        return self.last_response.response

    @property
    def tap_before_dosing(self) -> bool:
        """tap before dosing enabled"""
        self._warn_not_set('_tap_before_dosing', 'Tap before dosing')
        return self._tap_before_dosing

    @tap_before_dosing.setter
    def tap_before_dosing(self, value: bool):
        self._tap_before_dosing = value
        self.send_and_retrieve_response(f'QRD 1 1 1 {int(value)}')

    @property
    def tap_while_dosing(self) -> bool:
        """tap during dosing enabled"""
        self._warn_not_set('_tap_while_dosing', 'Tap while dosing')
        return self._tap_while_dosing

    @tap_while_dosing.setter
    def tap_while_dosing(self, value: bool):
        self._tap_while_dosing = value
        self.send_and_retrieve_response(f'QRD 1 1 2 {int(value)}')

    @property
    def tapper_intensity(self) -> int:
        """tapping intensity (percent)"""
        self._warn_not_set('_tapper_intensity', 'Tapper intensity')
        return self._tapper_intensity

    @tapper_intensity.setter
    def tapper_intensity(self, value: int):
        if type(value) is not int:
            raise TypeError(f'The tapper intensity must be an integer (provided: {type(value)}')
        if not 10 <= value <= 100:
            raise ValueError('The intensity value must be within the range 10-100')
        self._tapper_intensity = value
        self.send_and_retrieve_response(f'QRD 1 1 3 {value}')

    @property
    def tapper_duration(self) -> int:
        """duration for the tapper (seconds)"""
        self._warn_not_set('_tapper_duration', 'Tapper duration')
        return self._tapper_duration

    @tapper_duration.setter
    def tapper_duration(self, value: int):
        if type(value) is not int:
            raise TypeError(f'The tapper duration must be an integer (provided: {type(value)}')
        if not 1 <= value <= 10:
            raise ValueError('The tapper duration must be in the range 1-10')
        self._tapper_duration = value
        self.send_and_retrieve_response(f'QRD 1 1 4 {value}')

    @property
    def target_mass(self) -> float:
        """target mass (mg) for the Quantos"""
        self._warn_not_set('_target_mass', 'Target mass')
        return self._target_mass

    @target_mass.setter
    def target_mass(self, value: Union[float, int]):
        if type(value) not in [float, int]:
            raise TypeError(f'The target mass must be a float (provided: {type(value)}')
        if not 0.1 <= value <= 250000:
            raise ValueError('The target mass is outside of the range 0.1 - 250 000 mg')
        self._target_mass = float(value)
        self.send_and_retrieve_response(f'QRD 1 1 5 {value}')

    @property
    def tolerance_value(self) -> float:
        """tolerance for powder dosing in percent"""
        self._warn_not_set('_tolerance_percent', 'Tolerance value')
        return self._tolerance_percent

    @tolerance_value.setter
    def tolerance_value(self, value: float):
        if type(value) not in [float, int]:
            raise TypeError(f'The tolerance percent must be a float (provided: {type(value)}')
        if not 0.1 <= value <= 40.:
            raise ValueError('The tolerance percent is not in the range 0.1 - 40.')
        self._tolerance_percent = value
        self.send_and_retrieve_response(f'QRD 1 1 6 {value}')

    @property
    def tolerance_mode(self) -> str:
        """tolerance mode (+/- or 0/+)"""
        self._warn_not_set('_tolerance_mode', 'Tolerance mode')
        if self._tolerance_mode is not None:
            return self._tolerance_modes[self._tolerance_mode]

    @tolerance_mode.setter
    def tolerance_mode(self, value: Union[str, int]):
        if value in self._tolerance_modes:
            value = self._tolerance_modes.index(value)
        elif value not in [0, 1]:
            raise ValueError(f'The tolerance mode {value} is not recognized')
        self._tolerance_mode = value
        self.send_and_retrieve_response(f'QRD 1 1 7 {value}')

    @property
    def sample_id(self) -> str:
        """sample id"""
        self._warn_not_set('_sample_id', 'Sample ID')
        return self._sample_id

    @sample_id.setter
    def sample_id(self, value: str):
        if len(value) > 20:
            raise ValueError('The length of the sample ID exceeds the limit of 20 characters')
        self._sample_id = value
        self.send_and_retrieve_response(f'QRD 1 1 8 {value}')

    @property
    def user_id(self) -> str:
        """User ID"""
        self._warn_not_set('_user_id', 'User ID')
        return self._user_id

    @user_id.setter
    def user_id(self, value: str):
        if len(value) > 20:
            raise ValueError('The length of the user ID exceeds the limit of 20 characters')
        self._user_id = value
        self.send_and_retrieve_response(f'QRD 1 1 13 {value}')

    @property
    def dosing_algorithm(self) -> str:
        """Powder dosign algorithm (standard or advanced)"""
        self._warn_not_set('_algorithm', 'Powder dosing algorithm')
        if self._algorithm is not None:
            return self._algorithms[self._algorithm]

    @dosing_algorithm.setter
    def dosing_algorithm(self, value: Union[str, int]):
        if value.lower() in self._algorithms:
            value = self._algorithms.index(value.lower())
        elif value not in [0, 1]:
            raise ValueError(f'The algorithm selection {value} is not recognized')
        self._algorithm = value
        self.send_and_retrieve_response(f'QRD 1 1 14 {value}')

    @property
    def antistatic_enabled(self) -> bool:
        """AntiStatic kit enabled"""
        self._warn_not_set('_activate_antistatic', 'AntiStatic enabled')
        return self._activate_antistatic

    @antistatic_enabled.setter
    def antistatic_enabled(self, value: bool):
        self._activate_antistatic = value
        self.send_and_retrieve_response(f'QRD 1 1 15 {int(value)}')

    @property
    def front_door_position(self) -> str:
        """position of the front door"""
        return self.retrieve_response('QRD 2 3 7')

    @front_door_position.setter
    def front_door_position(self, value: Union[str, int]):
        positions = ['', '', 'close', 'open']
        if value.lower() in positions[2:]:
            value = positions.index(value.lower())
        elif value not in [2, 3]:
            raise ValueError(f'The front door position must be "open", "close" (or 2 or 3 respectively)')
        if self.wait_by_default is True:
            self.send_and_wait_for_completion(f'QRA 60 7 {value}')
        else:
            self.send_and_retrieve_response(f'QRA 60 7 {value}')

    @property
    def sampler_position(self) -> str:
        """position of the sampler head"""
        # todo consider returning integer position (this depends on how the user will use the information)
        pos = self.retrieve_response('QRD 2 3 8')
        if pos == '0':
            return 'home'
        return f'position {pos}'

    @sampler_position.setter
    def sampler_position(self, value: int):
        if type(value) is not int:
            raise TypeError('The sampler position must be an integer position')
        elif 0 <= value <= 30 is False:
            raise ValueError('The sampler position must be an integer between 0 and 30.')
        self.send_and_wait_for_completion(f'QRA 60 8 {value}')

    @property
    def sampler_status(self) -> str:
        """status of the Quantos autosampler"""
        return self.retrieve_response('QRD 2 2 8')

    @property
    def weigh_pan_status(self):
        """status of the weighing pan"""
        return self.retrieve_response('QRD 2 2 9')

    def set_pan_empty(self):
        """Tells the Quantos that the pan is empty"""
        self.send_and_retrieve_response('QRD 1 1 9 0')

    @property
    def head_data(self) -> DispensingHead:
        """contents of the RFID chip of the dose head"""
        # todo decide whether to return string or dict here
        self.send_and_wait_for_completion('QRD 2 4 11')
        return DispensingHead.create_from_string(
            self.last_xml_response
        )

    @property
    def sample_data(self) -> SampleData:
        """results data of the last dispense"""
        self.send_and_wait_for_completion('QRD 2 4 12')
        return SampleData.create_from_string(
            self.last_xml_response
        )

    def label_sample_data(self):
        """Triggers the Quantos to print a label with the last sample data"""
        self.send_and_retrieve_response('QRD 2 5 12')

    def protocol_sample_data(self):
        """Triggers the printing of the sample label through Quantos using the strip printer [sic]"""
        self.send_and_retrieve_response('QRD 2 6 12')

    def lock_dosing_pin_position(self):
        """Locks the dosing head to the dosing unit [sic]"""
        self.send_and_wait_for_completion('QRA 60 2 4')

    def unlock_dosing_pin_position(self):
        """Unlocks the dosing head from the dosing unit [sic]"""
        self.send_and_wait_for_completion('QRA 60 2 3')

    def start_dosing(self, wait_for: bool = None):
        """
        Starts the dosing with the set values

        :param wait_for: whether to wait for completion of the dosing before continuing.
        """
        # if wait is desired
        wait_for = wait_for if wait_for is not None else self.wait_by_default
        if wait_for is True:
            self.send_and_wait_for_completion('QRA 61 1')
        else:
            self.send_and_retrieve_response('QRA 61 1')

    def stop_dosing(self):
        """Stops the dosing process"""
        if self._sr_lock.locked() is True:
            self._sr_lock.release()
        self.send_and_retrieve_response('QRA 61 4')

    def cut_label(self):
        """cuts the label that has been printed"""
        self.send_and_retrieve_response('QRA 61 3')

    def wait_for(self):
        """
        Waits for the instrument to be not busy.

        The business of the instrument is determined by whether the
        send/receive lock is locked. This is an internal state of the API object which is locked upon a
        B response and unlocked on an A response.
        """
        while self.busy is True:
            pass

    def force_unlock(self):
        """
        Forces the API to be not busy. Take care using this method, as a hung locked state can indicate a communication
        error or a busy instrument. Forcing the state to be not busy can have unintended consequences in logic checks
        designed to control communication flow to and from the instrument.
        """
        self._sr_lock.release()
