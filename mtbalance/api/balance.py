import re
import socket
import logging
import threading
import time

from typing import Union, MutableMapping
from unithandler import UnitFloat

from ..commands import BalanceCommand, BalanceResponse

# expected finish characters
_TERMINATOR = '\r\n'

# default port for communication
DEFAULT_PORT = 8014

# default buffer size
BUFFER_SIZE = 2 ** 16

# regex match for ip and port
_ip_regex = re.compile(
    '\\b(?P<ip>(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.'
    '(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.'
    '(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.'
    '(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9]))\\b'
    '(:(?P<port>[0-9]{1,5})){0,1}'  # port
)

logger = logging.getLogger(__name__)


class Balance:
    DEFAULT_PORT = 8001

    # command values that remain unchanged (only need to be retrieved once)
    _unchanging: MutableMapping[str, Union[None, BalanceResponse]] = {
        'I2': None,
        'I3': None,
        'I4': None,
    }

    # response cycle time if there was no response
    RESPONSE_CYCLE_TIME = 0.1

    # wait time after executing a cancel to ensure the balance is finished its cancel operations
    #   (this appears to be required by a MT balance but causes unintended side-effects like closing the door
    #   unexpectedly for the quantos)
    WAIT_AFTER_CANCEL: float = 0.5

    def __init__(self,
                 address: str,
                 tcp_port: int = None,
                 logging_level: int = logging.INFO,
                 ):
        """
        Class for communicating with a Mettler Toledo SICS-enabled balance

        :param address: IP address for the balance
        :param tcp_port: port to connect to (default 8001)
        :param logging_level: logging level to use
        """
        self._address: str = None
        self._port: int = None
        self._last_response: BalanceResponse = None
        self._last_command: BalanceCommand = None

        self.address = address
        if self.port is None:  # ensures that a port specified in the address is not overwritten
            if tcp_port is None:  # if not set, use default
                tcp_port = DEFAULT_PORT
            self.port = tcp_port

        # create socket object
        self._tcp = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM,
        )
        # create send/receive lock
        self._sr_lock = threading.Lock()
        # connect
        self.logger: logging.Logger = logger
        self.connect()
        self.logger.setLevel(logging_level)

        # create and start response thread
        self._response_thread = threading.Thread(
            target=self._response_monitor,
            name=f'{self.__class__.__name__} TCP response thread'
        )
        self._response_thread.start()
        self._wait_for_response_to_command('I4')

    def __repr__(self):
        return f'{self.__class__.__name__}({self.address}:{self.port})'

    def __str__(self):
        return f'{self.device_type} ({self.address})'

    @property
    def address(self) -> str:
        """IP address"""
        return self._address

    @address.setter
    def address(self, value):
        match = _ip_regex.match(value)
        if match is None:
            raise ValueError(f'The IP address "{value}" is invalid')
        if match.group('port') is not None:
            self.port = match.group('port')
        self._address = match.group('ip')

    @property
    def port(self) -> int:
        """port for the address"""
        return self._port

    @port.setter
    def port(self, value):
        if type(value) is str:
            if value.isdigit() is False:
                raise TypeError(f'The port "{value}" is not a valid port')
            value = int(value)
        if type(value) is not int:
            raise TypeError(f'The port {value} is not a valid port')
        self._port = value

    @port.deleter
    def port(self):
        # reset to default port
        self._port = DEFAULT_PORT

    @property
    def last_command(self) -> BalanceCommand:
        """last command sent to the balance"""
        return self._last_command

    @property
    def last_response(self) -> BalanceResponse:
        """last response from the balance"""
        # # reenable if this becomes a problem (these lines are now accomplished by other methods)
        # if self._sr_lock.locked() is True:  # lock retrieval of the last response until the lock is released
        #     self.logger.info('The Quantos is returning a response, waiting for the response to complete.')
        #     while self._sr_lock.locked() is True:
        #         pass
        return self._last_response

    def connect(self):
        """
        Connects to the TCP server.
        """
        logger.debug(f'attempting connection to {self.address}:{self.port}')
        try:
            self._tcp.connect((
                self.address,
                self.port
            ))
            self._send_command('@')  # send cancel command
            time.sleep(self.WAIT_AFTER_CANCEL)
            # update logger as child
            self.logger = logger.getChild(f'{self.__class__.__name__}({self.address}:{self.port})')  # logger instance
            self.logger.debug('successfully connected')
        except ConnectionRefusedError as e:
            raise ConnectionRefusedError(
                f'{self.address}:{self.port} is reachable but a connection was actively refused: {e}'
            )
        except TimeoutError:
            raise ConnectionError(f'A connection could not be established with a Quantos at {self.address}:{self.port}')

    def __del__(self):
        """Destructor which ensures the connection is closed and response thread is closed"""
        self._tcp.close()

    def _response_monitor(self):
        """
        Method for checking the response of the instrument and printing messages to console.
        """
        while True:
            # retrieve response
            try:
                # we may receive multiple responses separated by \r\n
                responses = self._tcp.recv(BUFFER_SIZE).decode()
            except(TimeoutError, ConnectionResetError, ConnectionAbortedError) as e:
                self.logger.exception(f'Unable to communicate with the Quantos device at {self.address} ({e})')
                raise ConnectionError(f'A communication error was encountered: {e}')
                # sys.exit(1)

            if not responses:  # no response, continue loop
                time.sleep(self.RESPONSE_CYCLE_TIME)
                continue

            for response in responses.split('\r\n'):
                if response != '':
                    self.logger.debug(f'response received: {response.strip()}')
                    self._handle_response(response)  # handle response according to class specifications
                    # store unchanging value if not yet retrieved
                    if self._last_response.command in self._unchanging:
                        self._unchanging[self._last_response.command] = self.last_response

            if self._sr_lock.locked():  # todo check that this covers everything
                self._sr_lock.release()

    def _handle_response(self, response_string: str):
        """
        Performs the logic and actions associated with handling a response (triggered by response monitor method)

        :param response_string: response string received
        """
        response_string = response_string.strip()

        self._last_response = self._string_response_parse(response_string)
        # if there was an error
        if self._last_response.error is not None:
            # todo add instance flag to raise if True (otherwise just use logging)
            self.logger.error(self.last_response)

    @staticmethod
    def package_and_encode(command: str) -> bytes:
        """
        Packages and encodes a command string into a format appropriate for sending to the Quantos.

        :param command: string command to send
        :return: terminated and encoded command string
        """
        if command.endswith(_TERMINATOR) is False:
            command = command + _TERMINATOR
        return command.encode()

    @staticmethod
    def _string_command_parse(command_string: str) -> BalanceCommand:
        """
        Parses a command string into a BalanceCommand

        :param command_string: command string to parse
        :return: command class
        """
        return BalanceCommand(command_string)

    @staticmethod
    def _string_response_parse(response_string: str) -> BalanceResponse:
        """
        Parses a balance response string into a BalanceResponse

        :param response_string: response string to parse
        :return: response class
        """
        return BalanceResponse(response_string)

    def _send_command(self, cmd: Union[str, BalanceCommand]):
        """
        Sends the command to the Balance. Command structure is expected to conform to the Commands and Responses format.
        This method should not be used on its own since it sends and does not wait for a response. Users should interact
        with send_and_retrieve_response unless they have a specific reason to not wait for a response.

        :param cmd: correctly formatted command
        """
        if isinstance(cmd, BalanceCommand) is False:
            cmd = self._string_command_parse(cmd)
        self.logger.debug(f'sending "{cmd.command_with_value}" ({cmd})')
        self._last_command = cmd  # store last command
        self._sr_lock.acquire()  # acquire lock
        self._tcp.send(
            self.package_and_encode(cmd.command_with_value)
        )

    def send_and_retrieve_response(self,
                                   cmd: Union[str, BalanceCommand],
                                   response_command: Union[str, BalanceCommand] = None,
                                   ) -> BalanceResponse:
        """
        Sends a command and returns the response object retrieved.

        :param cmd: string command
        :param response_command: response command to wait for (if different from sent command)
        :return: parsed response object from the balance
        """
        if isinstance(cmd, BalanceCommand) is False:
            cmd = self._string_command_parse(cmd)
        prev_retrieved = self._retrieve_unchanging(cmd)
        if prev_retrieved is not None:
            return prev_retrieved
        # send the command to the balance
        self._send_command(cmd)
        # clear last response (prevents return of old response on repeat commands)
        self._last_response = None
        if response_command is None:
            response_command = cmd
        # wait for the return to match
        return self._wait_for_response_to_command(response_command)

    def _retrieve_unchanging(self, cmd: Union[str, BalanceCommand]) -> BalanceResponse:
        """
        If an unchanging command has already been retrieve, returns the previously retrieved response.

        :param cmd: command to search
        :return:
        """
        if isinstance(cmd, BalanceCommand) is False:
            cmd = self._string_command_parse(cmd)
        if cmd.command in self._unchanging and self._unchanging[cmd.command] is not None:
            return self._unchanging[cmd.command]

    def _wait_for_response_to_command(self, command: Union[str, BalanceCommand] = None) -> BalanceResponse:
        """
        Waits for a response to the provided command to appear.

        :param command: command to match
        :return: parsed response object from the balance
        """
        if command is None:
            command = self.last_command
        elif isinstance(command, BalanceCommand) is False:
            command = BalanceCommand(command)
        # wait for the balance to appropriately respond
        while self.last_response is None or self.last_response.is_response_to_command(command) is False:
            if self.last_response is not None and self.last_response.error is not None:
                raise ValueError(f'error with {self.last_response}: {self.last_response.response}')
        return self.last_response

    def retrieve_response(self, cmd: Union[str, BalanceCommand]) -> str:
        """
        Sends a command and returns the answer from the response.

        :param cmd: string command
        :return: answer value of the response from the balance
        """
        # return the answer from the returned response
        return self.send_and_retrieve_response(cmd).response

    @property
    def serial_number(self) -> str:
        """serial number of the balance"""
        return self.send_and_retrieve_response('I4').value.strip('"')

    @property
    def device_type(self) -> str:
        """device type and weight limit of the balance"""
        return self.send_and_retrieve_response('I2').value.strip('"')

    @property
    def stable_weight(self) -> UnitFloat:
        """current stable weight value with unit"""
        resp = self.send_and_retrieve_response('S')
        if resp.error is not None:
            raise ValueError(f'the stable weight could not be retrieved: {resp.error}')
        return resp.parsed_value

    @property
    def weight_immediately(self) -> UnitFloat:
        """retrieve the current weight immediately"""
        resp = self.send_and_retrieve_response('SI', 'S')
        if resp.error is not None:
            raise ValueError(f'the weight could not be retrieved: {resp.error}')
        # todo provide indication of whether this was stable
        return resp.parsed_value

    def tare(self) -> UnitFloat:
        """
        Tares the balance, returning the current tare value

        :return: the current tare value
        """
        resp = self.send_and_retrieve_response('T')
        if resp.error is not None:
            raise ValueError(f'a tare operation could not be executed: {resp.error}')
        return resp.parsed_value

    @property
    def current_tare_value(self) -> UnitFloat:
        """the current tare value of the balance"""
        resp = self.send_and_retrieve_response('TA')
        if resp.error is not None:
            raise ValueError(f'the current tare value could not be retrieved: {resp.error}')
        return resp.parsed_value

    def clear_tare_value(self):
        """clears the tare value of the balance"""
        self._send_command('TAC')

    def tare_immediately(self) -> UnitFloat:
        """
        Tares the balance immediately and independently of balance stability

        :return: current tare value
        """
        resp = self.send_and_retrieve_response('TI')
        if resp.error is not None:
            raise ValueError(f'the balance could not be tared: {resp.error}')
        return resp.parsed_value

    def zero(self):
        """zeros the balance reading"""
        resp = self.send_and_retrieve_response('Z')
        if resp.error is not None:
            raise ValueError(f'the balance could not be zeroed: {resp.error}')

    def zero_immediately(self):
        """zeros the balance immediately and independently of stability"""
        resp = self.send_and_retrieve_response('ZI')
        if resp.error is not None:
            raise ValueError(f'the balance could not be zeroed: {resp.error}')

    def cancel_operations(self):
        """cancels all current processes"""
        self.send_and_retrieve_response('@', 'I4')
        time.sleep(self.WAIT_AFTER_CANCEL)

    def write_text_on_screen(self, text: str):
        """
        Writes the provided text on the screen.

        :param text: text to write
        """
        if '"' in text:
            raise ValueError('double quotations may not be included in the provided text, use single quotations')
        self.send_and_retrieve_response(
            f'D "{text}"'
        )

    @property
    def FACT_enabled(self) -> bool:
        """whether FACT protocol is enabled"""
        # todo test on a supporting system
        return bool(int(self.send_and_retrieve_response('M75').value))

    @FACT_enabled.setter
    def FACT_enabled(self, value: bool):
        self.send_and_retrieve_response(
            f'M75 {int(value)}'
        )
