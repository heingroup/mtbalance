"""class for connecting to and operating with Zaber-augmented Quantos"""

import serial
import logging
import time

from .api import Quantos
from arduino_stepper.api import ArduinoStepper


logger = logging.getLogger(__name__)


class ArduinoAugment():
    def __init__(self,
                 serial_port: int,
                 perfect_counts: int = 1254307,
                 ):
        """
        Object for connecting and communicating with the Zaber augment of the Quantos

        :param serial_port: COM port of the MForce controller
        :param perfect_counts: the counts required for the perfect placement of the z-stage
        """
        self._counts: int = 0
        self._current_position: int = 0
        # open serial port (might go into connect method?)
        logger.info('connecting to Arduino controller')
        self.arduino = ArduinoStepper(
            motor_resolution=200,
            com_port=serial_port,
            baudrate=9600,
        )

        self.steps_for_perfect_movement = perfect_counts

    @property
    def steps_for_perfect_movement(self) -> int:
        """the steps required for the perfect movement of the instance"""
        return self._counts

    @steps_for_perfect_movement.setter
    def steps_for_perfect_movement(self, value: int):
        if value is not None:
            self._counts = value

    def set_home_direction(self, direction: int):
        logger.info('Setting home direction')
        self.arduino.home_direction(direction=direction)

    def home_z_stage(self):
        """homes the z stage"""
        logger.info('homing z-stage')
        self.arduino.home(block=True)
        self._current_position = 0

    def move_z_stage(self, steps: int = None, speed: float = 400):  # sign depends on motor direction
        """
        Moves the z_stage the precise amount it needs to

        :param steps: number of steps to move
        :param speed: the speed in steps/s
        """
        if steps is None:
            steps = self.steps_for_perfect_movement
        # TODO check if the unit has been homed at least once before allowing this method!
        # TODO add property self.positive_direction to make sure it's always moving in the right direction
        logger.info(f'moving z stage {steps} steps')
        self.arduino.rotate_steps(steps=steps, speed=speed, block=True)
        self._current_position += steps


class ArduinoAugmentedQuantos(Quantos, ArduinoAugment):
    def __init__(self,
                 address: str,
                 serial_port: int,
                 tcp_port: int = None,
                 logging_level: int = logging.INFO,
                 perfect_counts: int = 8700,
                 ):
        """
        Class for interacting with a Quantos which is augmented with a zaber stage controlled by an MForce controller.

        :param address: IP address of the instrument
        :param serial_port: COM port of the MForce controller
        :param serial_port: COM port of the MForce controller
        :param tcp_port: port (default 8014)
        :param logging_level: logging level for the logger used (passed through to logging)
        :param perfect_counts: the counts required for the perfect placement of the z-stage
        """
        Quantos.__init__(
            self,
            address=address,
            tcp_port=tcp_port,
            logging_level=logging_level,
        )
        ArduinoAugment.__init__(
            self,
            serial_port=serial_port,
            perfect_counts=perfect_counts,
        )
