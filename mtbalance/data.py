"""data structures for the Quantos"""
import xmltodict
import re
from unithandler.base import UnitFloat
from typing import Union
from collections import OrderedDict


# scalar for mg
_mg = UnitFloat('1 mg')
_percent = UnitFloat('1 %', scale_representation=False)
_seconds = UnitFloat('1 s')

_time_regex = re.compile('(?P<time>(?P<hour>2[0-3]|[01]?[0-9]):(?P<minute>[0-5]?[0-9]):(?P<second>[0-5]?[0-9]))')
_date_regex = re.compile('(?P<date>(?P<year>[012]\d{3})-(?P<month>0[1-9]|1[0-2])-(?P<day>0[1-9]|[12]\d|3[01]))')

# map of dictionary keys to SampleData kwargs
_XML_kwarg_map = {
    'Substance': 'substance',
    'Sample_ID': 'sample_id',
    'Lot_ID': 'lot_id',
    'User_ID': 'user_id',
    'Timestamp': 'timestamp',
    'Validity': 'validity',
    'Tapping_while_dosing': 'tap_while_dosing',
    'Tapping_before_dosing': 'tap_before_dosing',
    'Intensity': 'tapping_intensity',
    'Powder_dosing_mode': 'dosing_mode',
    'Content': 'quantity',
    'Target_quantity': 'target_quantity',
    'Tolerance': 'tolerance',
    'Accuracy': 'accuracy',
    'Duration': 'dose_duration',
    'Filling_date': 'filling_date',
    'Balance_Type': 'balance_type',
    'Last_cal.': 'last_calibrated',
    'Head_type': 'head_type',
    'Head_ID': 'head_id',
    'Rem._quantity': 'remaining_quantity',
    'Dose_limit': 'dose_limit',
    'Exp._date': 'expiry_date',
    'Levelcontrol': 'leveled',
    'Dosing_counter': 'number_of_doses',
    'Head_prod._date': 'head_production_date',
    'Retest_date': 'retest_date',
}


class DispensingHead(object):
    def __init__(self,
                 timestamp: str = None,  # todo also catch date and time separately
                 retest_date: str = None,
                 last_calibrated: str = None,
                 substance: str = None,
                 filling_date: str = None,
                 leveled: Union[str, bool] = None,
                 head_production_date: str = None,
                 head_type: str = None,
                 head_id: int = None,
                 balance_type: str = None,
                 dose_limit: int = None,
                 number_of_doses: int = None,
                 remaining_quantity: Union[str, float, UnitFloat] = None,
                 user_id: str = None,
                 expiry_date: str = None,
                 lot_id: int = None,
                 **kwargs,
                 ):
        """
        A class describing a Quantos dispensing head.

        :param timestamp: time stamp for the dosing (YYYY-MM-DD HH:MM:SS)
        :param retest_date: date when the sample was retested
        :param last_calibrated: last calibration date for the head
        :param substance: name for the substance dosed
        :param filling_date: date when the head was filled
        :param leveled: whether the balance is level
        :param head_production_date: production date for the dosing head
        :param head_type: type of head
        :param head_id: ID # for the head
        :param balance_type: type of balance
        :param dose_limit: limit for dosing  todo figure out if this is mg or number of doses
        :param number_of_doses: number of doses dispensed
        :param remaining_quantity: remaining quantity in the head
        :param user_id: current user ID
        :param expiry_date: expiry date for the substance
        :param lot_id: lot ID for the sample
        :param kwargs: catch for extra kwargs
        """
        self._date = None
        self._time = None
        self._retest_date = None
        self._filling_date = None
        self._lot_id = None
        self._last_calibrated = None
        self._leveled = None
        self._production_date = None
        self._head_type = None
        self._head_id = None
        self._dose_limit = None
        self._n_doses = None
        self._remaining_quantity = None

        # straight attribute setting
        self.substance: str = substance  # todo consider implementing molecule?
        self.balance_type: str = balance_type
        self.user_id: str = user_id
        self.expiry_date: str = expiry_date  # todo convert to date catch

        # parsing required
        self.timestamp = timestamp
        self.retest_date = retest_date
        self.last_calibrated = last_calibrated
        self.filling_date = filling_date
        self.leveled = leveled
        self.production_date = head_production_date
        self.head_type = head_type
        self.head_id = head_id
        self.dose_limit = dose_limit
        self.number_of_doses = number_of_doses
        self.remaining_quantity = remaining_quantity
        self.lot_id = lot_id

    def __repr__(self):
        return f'{self.__class__.__name__}({self.head_type} {self.substance} {self.remaining_quantity})'

    def __str__(self):
        return f'{self.head_type} containing {self.remaining_quantity} {self.substance}'

    @property
    def date(self) -> str:
        """date of the dispense"""
        return self._date

    @date.setter
    def date(self, value: str):
        if value is not None:
            match = _date_regex.search(value)
            if match is None:
                raise ValueError(f'The date "{value}" is not a valid YYYY-MM-DD formatted date')
            self._date = match.group('date')

    @property
    def time(self) -> str:
        """time stamp for the dispense"""
        return self._time

    @time.setter
    def time(self, value: str):
        if value is not None:
            match = _time_regex.search(value)
            if match is None:
                raise ValueError(f'The time "{value}" is not a valid HH:MM:SS formatted time')
            self._time = match.group('time')

    @property
    def timestamp(self) -> str:
        """time stamp for the dispensing"""
        return f'{self.date} {self.time}'

    @timestamp.setter
    def timestamp(self, value: str):
        if value is not None:
            self.date = value
            self.time = value

    @property
    def retest_date(self) -> str:
        """date when the sample was retested"""
        return self._retest_date

    @retest_date.setter
    def retest_date(self, value: str):
        if value is not None:
            match = _date_regex.search(value)
            if match is None:
                raise ValueError(f'The date "{value}" is not a valid YYYY-MM-DD formatted date')
            self._retest_date = match.group('date')

    @property
    def last_calibrated(self) -> str:
        """Date of the last calibration"""
        return self._last_calibrated

    @last_calibrated.setter
    def last_calibrated(self, value: str):
        if value is not None:
            match = _date_regex.search(value)
            if match is None:
                raise ValueError(f'The date "{value}" is not a valid YYYY-MM-DD formatted date')
            self._last_calibrated = match.group('date')

    @property
    def production_date(self) -> str:
        """production date for the head"""
        return self._production_date

    @production_date.setter
    def production_date(self, value: str):
        if value is not None:
            match = _date_regex.search(value)
            if match is None:
                raise ValueError(f'The date "{value}" is not a valid YYYY-MM-DD formatted date')
            self._production_date = match.group('date')

    @property
    def filling_date(self) -> str:
        """date that the dosing head was filled on"""
        return self._filling_date

    @filling_date.setter
    def filling_date(self, value):
        if value is not None:
            match = _date_regex.search(value)
            if match is None:
                raise ValueError(f'The date "{value}" is not a valid YYYY-MM-DD formatted date')
            self._filling_date = match.group('date')

    @property
    def lot_id(self) -> int:
        """lot ID for the sample"""
        return self._lot_id

    @lot_id.setter
    def lot_id(self, value: int):
        if value is not None:
            self._lot_id = int(value)

    @property
    def leveled(self) -> bool:
        """whether the balance is leveled"""
        return self._leveled

    @leveled.setter
    def leveled(self, value: Union[str, bool]):
        if value is not None:
            if type(value) is str:
                if value.lower() == 'balance is leveled':
                    value = True
                else:
                    value = False
            self._leveled = value

    @property
    def head_type(self) -> str:
        """dosing head type"""
        return self._head_type

    @head_type.setter
    def head_type(self, value: str):
        if value is not None:
            self._head_type = value

    @property
    def head_id(self) -> int:
        """ID # for the dosing head"""
        return self._head_id

    @head_id.setter
    def head_id(self, value: Union[str, int]):
        if value is not None:
            if type(value) is str:
                value = int(value)
            self._head_id = value

    @property
    def dose_limit(self) -> int:
        """maximum number of doses for the head"""
        return self._dose_limit

    @dose_limit.setter
    def dose_limit(self, value: Union[str, int]):
        if value is not None:
            if type(value) is str:
                value = int(value)
            self._dose_limit = value

    @property
    def number_of_doses(self) -> int:
        """number of doses dispensed by the head"""
        return self._n_doses

    @number_of_doses.setter
    def number_of_doses(self, value: Union[str, int]):
        if value is not None:
            if type(value) is str:
                value = int(value)
            self._n_doses = value

    @property
    def remaining_quantity(self) -> UnitFloat:
        return self._remaining_quantity

    @remaining_quantity.setter
    def remaining_quantity(self, value: Union[str, float, UnitFloat]):
        if value is not None:
            if isinstance(value, UnitFloat):
                self._remaining_quantity = value
            else:
                self._remaining_quantity = UnitFloat(f"{float(value['#text'])} {value['@Unit']}")
            # if there is no unit, apply default unit
            if self._remaining_quantity.unit == '':
                self._remaining_quantity *= _mg

    @property
    def lot_id(self):
        """lot ID for the sample"""
        return self._lot_id

    @lot_id.setter
    def lot_id(self, value):
        if value is not None:
            self._lot_id = value

    @classmethod
    def create_from_string(cls, xml_string: str) -> 'DispensingHead':
        """
        Constructs a class instance from the provided string form of an XML file

        :param xml_string: string form of XML file
        :return: SampleData object
        """
        parsed = xmltodict.parse(xml_string)
        # look for subkeys in parsed XML (these are the returned keys from the Quantos)
        if 'Info_head' in parsed:
            parsed = parsed['Info_head']
        elif 'Dosing' in parsed:
            parsed = parsed['Dosing']
        return cls.create_from_dict(parsed)

    @classmethod
    def create_from_xml(cls, xml_file_path: str) -> 'DispensingHead':
        """
        Constructs a class instance from the provided XML file

        :param xml_file_path: file path to the XML file
        :return: SampleData object
        """
        with open(xml_file_path, 'rt') as open_file:
            return cls.create_from_string(
                open_file.read()
            )

    @classmethod
    def create_from_dict(cls, dct: dict) -> 'DispensingHead':
        """
        Constructs a class instance from a provided dictionary. The dictionary keys are expected to conform to the
        xml dictionary map generated by the Quantos or use the expected keyword arguments for instantiating a
        SampleData instance.

        :param dct: dictionary of instantiation keyword arguments
        :return: SampleData object
        """

        def get_value(key):
            """
            Retrieves a value if present
            Also digs one layer in to retrieve a value from an OrderedDict generated with xmltodict
            """
            if key in dct:
                if type(dct[key]) is OrderedDict:
                    if '@Unit' in dct[key]:
                        return UnitFloat(
                            value=dct[key]['#text'],
                            unit=dct[key]['@Unit'],
                        )
                else:
                    return dct[key]

        kwargs = {}
        # retrieve the easily mapped values
        for xml_key, kwarg in _XML_kwarg_map.items():
            if xml_key in dct:
                kwargs[kwarg] = get_value(xml_key)

        return cls(
            **kwargs,
            **dct,  # insert dictionary
        )


class SampleData(DispensingHead):
    def __init__(self,
                 sample_id: str = None,
                 quantity: Union[str, float, UnitFloat] = None,
                 target_quantity: Union[str, float, UnitFloat] = None,
                 tolerance: Union[str, float, UnitFloat] = None,
                 validity: str = None,
                 accuracy: Union[str, float, UnitFloat] = None,
                 dose_duration: Union[str, float, UnitFloat] = None,
                 tap_while_dosing: bool = None,
                 tap_before_dosing: bool = None,
                 tapping_intensity: int = None,
                 dosing_mode: str = None,

                 # kwargs for DispensingHead
                 timestamp: str = None,  # todo also catch date and time separately
                 retest_date: str = None,
                 last_calibrated: str = None,
                 substance: str = None,
                 filling_date: str = None,
                 leveled: Union[str, bool] = None,
                 head_production_date: str = None,
                 head_type: str = None,
                 head_id: int = None,
                 balance_type: str = None,
                 dose_limit: int = None,
                 number_of_doses: int = None,
                 remaining_quantity: Union[str, float, UnitFloat] = None,
                 user_id: str = None,
                 expiry_date: str = None,
                 lot_id: int = None,
                 **kwargs,  # pass through kwargs to DispensingHead
                 ):
        """
        A class describing a sample dosing event performed by the Quantos.

        :param sample_id: name for the dosing event
        :param user_id: ID for the user controlling the Quantos during the dosing
        :param quantity: quantity dosed
        :param target_quantity: target quantity for dosing
        :param tolerance: tolerance for the dosing (percent)
        :param validity: validity flag for the dosing
        :param accuracy: accuracy for the dosing (percent)
        :param dose_duration: duration of the dosing
        :param tap_while_dosing: whether tapping was enabled during dosing
        :param tap_before_dosing: whether tapping was enabled before dosing
        :param tapping_intensity: intensity scalar for the tapping
        :param dosing_mode: dosing mode for the dosing event
        :param timestamp: time stamp for the dosing (YYYY-MM-DD HH:MM:SS)
        :param retest_date: date when the sample was retested
        :param last_calibrated: last calibration date for the head
        :param substance: name for the substance dosed
        :param filling_date: date when the head was filled
        :param leveled: whether the balance is level
        :param head_production_date: production date for the dosing head
        :param head_type: type of head
        :param head_id: ID # for the head
        :param balance_type: type of balance
        :param dose_limit: limit for dosing  todo figure out if this is mg or number of doses
        :param number_of_doses: number of doses dispensed
        :param remaining_quantity: remaining quantity in the head
        :param user_id: current user ID
        :param expiry_date: expiry date for the substance
        :param lot_id: lot ID for the sample
        """
        DispensingHead.__init__(
            self,
            timestamp=timestamp,
            retest_date=retest_date,
            last_calibrated=last_calibrated,
            substance=substance,
            filling_date=filling_date,
            leveled=leveled,
            head_production_date=head_production_date,
            head_type=head_type,
            head_id=head_id,
            balance_type=balance_type,
            dose_limit=dose_limit,
            number_of_doses=number_of_doses,
            remaining_quantity=remaining_quantity,
            user_id=user_id,
            expiry_date=expiry_date,
            lot_id=lot_id,
            **kwargs,
        )

        self._quantity = None
        self._target_quantity = None
        self._tolerance = None
        self._accuracy = None
        self._dose_duration = None
        self._tap_during_dosing = None
        self._tap_before_dosing = None
        self._tapping_intensity = None

        # straight attribute setting
        self.sample_id: str = sample_id
        self.dosing_mode: str = dosing_mode
        self.validity: str = validity

        # require parsing (setting through properties)
        self.quantity = quantity
        self.target_quantity = target_quantity
        self.tolerance = tolerance
        self.accuracy = accuracy
        self.dose_duration = dose_duration
        self.tap_before_dosing = tap_before_dosing
        self.tap_while_dosing = tap_while_dosing
        self.tap_intensity = tapping_intensity

    def __str__(self):
        return f'{self.quantity} {self.substance} ({self.accuracy} accuracy)'

    def __repr__(self):
        return f'{self.__class__.__name__}({self.quantity} {self.substance} {self.accuracy})'

    @property
    def quantity(self) -> UnitFloat:
        """quantity dispensed"""
        return self._quantity

    @quantity.setter
    def quantity(self, value: Union[str, float, UnitFloat]):
        if value is not None:
            if isinstance(value, UnitFloat):
                self._quantity = value
            else:
                self._quantity = UnitFloat(value)
            # if there is no unit, apply default unit
            if self._quantity.unit == '':
                self._quantity *= _mg

    @property
    def target_quantity(self) -> UnitFloat:
        """target quantity for the dispense"""
        return self._target_quantity

    @target_quantity.setter
    def target_quantity(self, value: Union[float, UnitFloat]):
        if value is not None:
            if isinstance(value, UnitFloat):
                self._target_quantity = value
            else:
                self._target_quantity = UnitFloat(value)
            # if there is no unit, apply default unit
            if self._target_quantity.unit == '':
                self._target_quantity *= _mg

    @property
    def tolerance(self) -> UnitFloat:
        """tolerance (in percent) for the the dispensing operation"""
        return self._tolerance

    @tolerance.setter
    def tolerance(self, value: Union[float, UnitFloat]):
        if value is not None:
            if isinstance(value, UnitFloat):
                self._tolerance = value
                self._tolerance.scale_representation = False
            else:
                self._tolerance = UnitFloat(
                    value,
                    scale_representation=False,
                )
            # if there is no unit, apply default unit
            if self._tolerance.unit == '':
                self._tolerance.unit = '%'

    @property
    def accuracy(self) -> UnitFloat:
        """accuracy of the dispense (percent)"""
        return self._accuracy

    @accuracy.setter
    def accuracy(self, value: Union[float, UnitFloat]):
        if value is not None:
            if isinstance(value, UnitFloat):
                self._accuracy = value
                self._accuracy.scale_representation = False
            else:
                self._accuracy = UnitFloat(
                    value,
                    scale_representation=False,
                )
            # if there is no unit, apply default unit
            if self._accuracy.unit == '':
                self._accuracy.unit = '%'

    @property
    def dose_duration(self) -> UnitFloat:
        """the duration of the dosing"""
        return self._dose_duration

    @dose_duration.setter
    def dose_duration(self, value: Union[float, UnitFloat]):
        # todo catch string, float, or UnitFloat
        if value is not None:
            if isinstance(value, UnitFloat):
                self._dose_duration = value
            else:
                self._dose_duration = UnitFloat(value)
            # if there is no unit, apply default unit
            if self._dose_duration.unit == '':
                self._dose_duration *= _seconds

    @property
    def tap_while_dosing(self) -> bool:
        """whether tapping while dosing was enabled"""
        return self._tap_during_dosing

    @tap_while_dosing.setter
    def tap_while_dosing(self, value: Union[str, bool]):
        if type(value) is str:
            if value.lower() not in ['on', 'off']:
                raise ValueError(f'The value "{value}" is not valid')
            self._tap_during_dosing = True if value.lower() == 'on' else False
        elif type(value) is bool:
            self._tap_during_dosing = value

    @property
    def tap_before_dosing(self) -> bool:
        """wehther tapping before dosing was enabled"""
        return self._tap_before_dosing

    @tap_before_dosing.setter
    def tap_before_dosing(self, value: Union[str, bool]):
        if type(value) is str:
            if value.lower() not in ['on', 'off']:
                raise ValueError(f'The value "{value}" is not valid')
            self._tap_before_dosing = True if value.lower() == 'on' else False
        elif type(value) is bool:
            self._tap_before_dosing = value

    @property
    def tap_intensity(self) -> int:
        """tapping intensity for the dosing"""
        return self._tapping_intensity

    @tap_intensity.setter
    def tap_intensity(self, value: int):
        if value is not None:
            self._tapping_intensity = int(value)

    def write_to_json(self, target_path: str):
        """
        Saves the instance attributes as a JSON file.

        :param target_path: target path for the the JSON file
        """
        pass  # todo
