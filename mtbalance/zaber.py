"""class for connecting to and operating with Zaber-augmented Quantos"""

import serial
import logging
import time

from .api import Quantos


logger = logging.getLogger(__name__)


class ZaberAugment:
    def __init__(self,
                 serial_port: str,
                 perfect_counts: int = 1254307,
                 ):
        """
        Object for connecting and communicating with the Zaber augment of the Quantos

        :param serial_port: COM port of the MForce controller
        :param perfect_counts: the counts required for the perfect placement of the z-stage
        """
        self._counts: int = None
        # open serial port (might go into connect method?)
        logger.info('connecting to MForce controller')
        self._mforce = serial.Serial(port=serial_port)  # TODO deal with exceptions

        # send basic config info
        logger.debug('setting device name and echo mode')
        self._mforce.write('DN=\"1\"\r'.encode())  # set device name to "1"
        self._mforce.write('EM=2\r'.encode())  # set echo mode to no response
        self.steps_for_perfect_movement = perfect_counts
        # TODO check if settings look correct otherwise

    @property
    def steps_for_perfect_movement(self) -> int:
        """the steps required for the perfect movement of the instance"""
        return self._counts

    @steps_for_perfect_movement.setter
    def steps_for_perfect_movement(self, value: int):
        if value is not None:
            self._counts = value

    def home_z_stage(self):
        """homes the z stage"""
        logger.info('homing z-stage')
        self._mforce.write('HM 3\r'.encode())  # homing mode depends on positive direction, refer to manual p.66
        while True:
            time.sleep(1)  # TODO make more granular
            self._mforce.write('PR MV\r'.encode())  # check moving flag
            reply = self._mforce.read_until().decode()
            if '0' in reply:  # if not moving, break the loop
                break
            self._mforce.write('P=0\r'.encode())  # reset position counter to 0

    def move_z_stage(self, steps: int = None):  # sign depends on motor direction
        """
        Moves the z_stage the precise amount it needs to

        :param steps: number of steps to move
        """
        if steps is None:
            steps = self.steps_for_perfect_movement
        # TODO check if the unit has been homed at least once before allowing this method!
        # TODO add property self.positive_direction to make sure it's always moving in the right direction
        logger.info(f'moving z stage {steps} steps')
        self._mforce.write(f'MA {int(steps)},1\r'.encode())  # move to absolute position, report back when done
        while True:
            time.sleep(1)
            reply = self._mforce.read_until().decode()
            if '1' in reply:  # TODO make device name a property
                break


class ZaberAugmentedQuantos(Quantos, ZaberAugment):
    def __init__(self,
                 address: str,
                 serial_port: str,
                 tcp_port: int = None,
                 logging_level: int = logging.INFO,
                 perfect_counts: int = 1254307,
                 ):
        """
        Class for interacting with a Quantos which is augmented with a zaber stage controlled by an MForce controller.

        :param address: IP address of the instrument
        :param serial_port: COM port of the MForce controller
        :param serial_port: COM port of the MForce controller
        :param tcp_port: port (default 8014)
        :param logging_level: logging level for the logger used (passed through to logging)
        :param perfect_counts: the counts required for the perfect placement of the z-stage
        """
        Quantos.__init__(
            self,
            address=address,
            tcp_port=tcp_port,
            logging_level=logging_level,
        )
        ZaberAugment.__init__(
            self,
            serial_port=serial_port,
            perfect_counts=perfect_counts,
        )
