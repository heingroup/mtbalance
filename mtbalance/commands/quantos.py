import re

# error code and meaning map
from typing import Union

from .balance import BalanceCommand, BalanceResponse, A_RESPONSES


ERROR_MAP = {
    -1: 'Command understood but was not executable due to an incorrect parameter',
    1: 'Not Mounted',  # todo verify always the case
    2: 'Another job is running.',
    3: 'Timeout',
    4: 'Not selected',  # todo verify
    5: 'Not allowed at this moment',
    6: 'Weight not stable',
    7: 'Powder flow error',
    8: 'Stopped by external action',
    9: 'Safe position error',
    10: 'Head not allowed',
    11: 'Head limit reached',
    12: 'Head expiry date reached',
    13: 'Sampler blocked',
}

B_RESPONSES = {  # B response which is most commonly used
    0: 'Executing command',
    1: 'Command understood, sending XML',
}
# todo consider putting value types in the command dictionary for verification

# command hierarchy map
COMMAND_MAP = {
    # todo separate into QRA/D dictionaries
    1: {  # setter commands
        1: {
            # 'A': 'Setting stored',  # generic response for set
            1: {
                'name': 'set value tapping before dosing',
                'A': A_RESPONSES[1],
                # 0: 'Tap before dosing deactivated',
                # 1: 'Tap before dosing activated',
            },
            2: {
                'name': 'set value tapping while dosing',
                'A': A_RESPONSES[1],
            },
            3: {
                'name': 'set the tapper intensity',
                'A': 'Intensity setting stored',
            },
            4: {
                'name': 'set tapper duration',
                'A': A_RESPONSES[1],
            },
            5: {
                'name': 'set target value in mg',
                'A': A_RESPONSES[1],
            },
            6: {
                'name': 'set target tolerance value in percent',
                'A': A_RESPONSES[1],
            },
            7: {
                'name': 'set tolerance mode',
                'A': A_RESPONSES[1],
            },
            8: {
                'name': 'set value sample id',
                'A': A_RESPONSES[0],
            },
            9: {  # defines whether pan is empty
                'name': 'set value pan',
                'A': A_RESPONSES[0],
                'B': B_RESPONSES[0],
            },
            13: {
                'name': 'set value user id',
                'A': A_RESPONSES[0],
            },
            14: {
                'name': 'set algorithm selection',
                'A': A_RESPONSES[0],
            },
            15: {
                'name': 'set antistatic kit',
                'A': A_RESPONSES[1],
            }
        },
    },
    2: {  # getter commands
        2: {
            8: {
                'name': 'get sampler status',
                'A': 'sampler is switched',
                0: 'off/disabled',
                1: 'on/selected',
            },
            9: {
                'name': 'get weighting pan status',
                'A': 'pan is',
                0: 'empty',
                1: 'not empty',
            },
        },
        3: {
            7: {
                'name': 'enquiry of the front door position',
                'A': 'front door position',
                2: 'closed',
                3: 'open',
                8: 'not detectable',
                9: 'running',
            },
            8: {
                'name': 'enquiry of the sampler position',
                'A': 'sampler position',
                0: 'home position',
                # todo figure out how to parse the 1-30 position values
            },
        },
        4: {
            11: {
                'name': 'get head data',
                'B': B_RESPONSES[1],
                'A': 'xml send complete',
            },
            12: {
                'name': 'get sample data',
                'B': B_RESPONSES[1],
                'A': 'xml send complete',
            }
        },
        5: {
            12: {  # todo finish, regex will fail on this because the answer value is a string (not int)
                'name': 'get label sample data',

            }
        },
        6: {
            12: {  # todo finish (how is this different from 2 5 12?)
                'name': 'print sample label'
            }
        },
    },
    20: {  # user I/O shutoff
        'name': 'activate/deactivate user input of value/text',
        # todo figure out how to parse this
        #   it looks like this is a continuous process
    },
    49: {  # message window shutoff
        # todo figure out what to do with this
        #   this is a command for issuing a popup window on the quantos... perhaps a class?
    },
    60: {  # move commands
        2: {
            'name': 'move dosing head pin',
            # todo define positions here and parse
            'B': B_RESPONSES[0],
        },
        7: {
            'name': 'move front door',
            'A': A_RESPONSES[0],
            'B': B_RESPONSES[0],
        },
        8: {
            'name': 'move autosampler position',
            'A': A_RESPONSES[0],
            'B': B_RESPONSES[0],
        }
    },
    61: {  # DO commands
        1: {
            'name': 'start dosing process',
            'A': A_RESPONSES[0],
            'B': B_RESPONSES[0],
        },
        3: {
            'name': 'cut label',
            'A': A_RESPONSES[0],
            'B': B_RESPONSES[0],
        },
        4: {
            'name': 'stop dosing process',
            'A': A_RESPONSES[0],
            'B': B_RESPONSES[0],
        }
    },
}
# regex for prefix retrieval
_prefix_regex = re.compile('(?P<prefix>QR[AD])')
# regex for command pattern only
_cmd_regex = re.compile(
    f'{_prefix_regex.pattern}'  # command prefix
    '( (?P<T1>[\d]{0,2}))'  # tier-one parameter (one to two digits)
    '( (?P<T2>\d))?'  # tier-two parameter (single digit)
    '( (?P<T3>\d{1,2}))?'  # T3 param (one to two digits)
    '( (?P<T4>[A-Z0-9\.]{1,9}))?'  # T4 parameter (one upper character or one to nine digits - max is target mass)
)
_VALUE_REGEX_STRING = '( (?P<value>[^\s]+(?:\s[^\s]+)*))?'
TIER_KEYS = ['T1', 'T2', 'T3', 'T4']


_RESPONSE_SUFFIX = (
    '('
    '(?P<error> I (?P<error_code>\d))'  # catch for error code
    '|(?P<answer>( (?P<answer_code>\d))? A)'  # catch for answer w/ or w/o code
    '|(?P<pattern_error> L)'  # pattern error
    '|(?P<in_process> B)'  # command in process
    '|)'
)


def get_subdictionary(indicies: list) -> dict:
    """gets the subdictionary from the command map by indicies"""
    dct = COMMAND_MAP
    for ind in indicies:
        dct = dct[ind]
    return dct


class QuantosCommand(BalanceCommand):
    _base_regex = _cmd_regex

    def __init__(self,
                 command_string: str,
                 argument: str = None,
                 ):
        """
        Object describing a command string to be sent to a Quantos

        :param command_string: string command
        :param argument: argument to send with the command
        """
        self._indicies = []
        BalanceCommand.__init__(
            self,
            command_string=command_string,
            argument=argument,
        )

    def __str__(self):
        return (
            f'{self.name}'
            f'{f" {self._value}" if self._value is not None else ""}'
        )

    def __repr__(self):
        return self.__str__()

    def __bytes__(self):
        return bytes(self.command)

    def __add__(self, other: str):
        return str(self) + other

    def __radd__(self, other):
        return other + str(self)

    def __eq__(self, other):
        if isinstance(other, QuantosCommand):
            return self.command_with_value == other.command_with_value
        elif type(other) is str:
            return other.strip() == self.command_with_value
        else:
            raise TypeError('Equality check is only supported for str and QuantosCommand types')

    def encode(self, encoding, errors):
        return self.command.encode(encoding, errors)

    @property
    def command(self) -> str:
        """command portion of the command string"""
        return (
            f'{self._prefix} '  # prefix
            f'{" ".join(str(ind) for ind in self._indicies)}'  # indicies
        )

    @command.setter
    def command(self, value: str):
        self._parse_command(value)

    def _parse_command(self, command_string: str):
        """
        Parses the provided command

        :param command_string: command string to parse
        """
        super(QuantosCommand, self)._parse_command(command_string)
        dct = COMMAND_MAP
        key_iterator = iter(TIER_KEYS)  # create generator to retrieve the next key if not None
        for key in key_iterator:
            value = self._match.group(key)
            if value is not None:
                if value.isnumeric():
                    ind = int(value)
                    if ind not in dct:
                        break
                    if type(dct[ind]) == dict:
                        dct = dct[ind]
                        self._indicies.append(ind)
                        # regex_string += f' (?P<{key}>{ind})'
            # once a named dictionary is reached, that is the terminus
            if 'name' in dct:
                # todo consider using 'tier_type' key for every dictionary level (more explicit)
                #   e.g. 'tier', 'command'
                # check if there is a value for the next tier (this value should be the value of the command)
                key = next(key_iterator)
                value = self._match.group(key)
                if value is not None:
                    self.value = value
                break

    @property
    def command_with_value(self) -> str:
        return self.command + f'{f" {self._value}" if self._value is not None else ""}'

    @property
    def dct(self) -> dict:
        return get_subdictionary(self._indicies)

    @property
    def name(self):
        if 'name' not in self.dct:
            return 'UNDEFINED'
        return self.dct['name']

    def match(self, string: str):
        """
        Returns a regex match of the provided string with the instance's regex pattern

        :param string: string to match
        :return: regex Match object (re.Match)
        """
        # todo figure out if this is needed and reimplement if so
        #   - instance-specific regex will need to be determined for each class
        raise NotImplementedError
        return self._regex.match(string)


class QuantosResponse(QuantosCommand, BalanceResponse):
    def __init__(self, response_string: str):
        """
        Object for interpreting a Quantos response string

        :param response_string: string response received from the Quantos
        """
        self._error = False
        QuantosCommand.__init__(self, response_string)
        self.response_string = response_string

    def __str__(self):
        if self.error_code is not None:
            return (
                f'Error {self.error_code} with {self.name}'
                f' ({self.command})'
                f': {self.error}'
            )
        elif self.response_code is not None:
            return f'{self.name}: {self.response}'

    def __bytes__(self):
        raise NotImplementedError(f'bytes casting is not implemented for {self.__class__.__name__}')

    def _parse_command(self, command_string: str):
        """
        Parses the provided command

        :param command_string: command string to parse
        """
        super(QuantosResponse, self)._parse_command(command_string)
        match = re.match(
            self.command + _RESPONSE_SUFFIX,
            command_string
        )
        self._match = match
        if match.group('error') is not None:  # error response
            self._error = True
            self._response_code = int(match.group('error_code'))
        elif match.group('answer') is not None:  # answer response
            if match.group('answer_code') is not None:
                answer_code = int(match.group('answer_code'))
            else:
                answer_code = 'A'
            self._response_code = answer_code
        elif " L" in command_string:  # pattern issue
            self._error = True
            self._response_code = -1  # special case for bad parameter
        elif match.group('in_process') is not None:
            self._response_code = 'B'
        else:
            raise ValueError(f'The response structure of "{command_string}" was not understood')

    def encode(self, encoding, errors):
        raise NotImplementedError(f'encoding is not implemented for {self.__class__.__name__}')

    def __eq__(self, other):
        if isinstance(other, QuantosResponse):
            return all([
                self.command == other.command,
                self.error_code == other.error_code,
                self.response_code == other.response_code,
            ])
        elif isinstance(other, QuantosCommand):  # command prefix
            return self.command == other.command
        elif type(other) is str:
            return self.response_string.strip() == other.strip()
        else:
            raise TypeError('Equality comparison is only supported for str, QuantosCommand, and QuantosResponse types.')

    @property
    def error(self) -> str:
        """Error indicated by the response"""
        code = self.error_code
        if code is not None:
            return ERROR_MAP[self._response_code]

    @property
    def error_code(self) -> Union[int, str]:
        """error code indicated by the response"""
        if self._error is True:
            return self._response_code

    @property
    def response_interpretation(self) -> str:
        """string interpretation of the response"""
        out = ''
        if self._response_code != 'B':  # special "in process" case
            out += self.dct['A']
        if self._response_code is not None:
            out += f' {self.dct[self._response_code]}'
        return out

    def is_response_to_command(self, command: Union[str, QuantosCommand]) -> bool:
        """
        Checks whether the supplied command is a response to the provided command.

        :param command: command to check against
        :return: match
        """
        if isinstance(command, QuantosCommand) is False:
            command = QuantosCommand(command)
        return self.response_string.startswith(command.command)
