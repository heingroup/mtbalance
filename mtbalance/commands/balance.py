"""
Generic command and response for MT balances
"""

import re
from unithandler import UnitFloat
from typing import Union

A_RESPONSES = {  # repetitive A response types
    0: 'Command executed and complete',
    1: 'Setting stored',
}

_NOT_EXECUTABLE = 'command understood but was not executable'

_GENERAL_ERROR = (  # general balance something-went-wrong error
    f'{_NOT_EXECUTABLE} (balance is currently executing another command, e.g., '
    'taring, or timeout as stability was not reached).'
)


_OVERLOAD_MESSAGE = 'balance in overload range'
_UNDERLOAD_MESSAGE = 'balance in underload range'


SUPPORTED_COMMANDS = {
    # todo support and parse I0 command
    '@': {
        'name': 'cancel all current processes',
        # responds with serial number
    },
    'ES': {
        'name': 'not recognized or not allowed',
        None: 'syntax error: the balance has not recognized the received command or the command is not allowed',
    },
    'ET': {
        'name': 'faulty command',
        None: 'the balance has received a "faulty" command, e.g. owing to a parity error or interface break',
    },
    'EL': {
        'name': 'could not execute',
        None: 'the balance could not execute the received command'
    },
    'I2': {
        'name': 'device data',
        'A': 'balance type and capacity',
        'I': _NOT_EXECUTABLE,
    },
    'I3': {
        'name': 'device type and capacity',
        'A': 'balance software version and type definition number',
    },
    'I4': {
        'name': 'serial number of the balance',
        'I': _GENERAL_ERROR,
    },
    'D': {
        'name': 'write text to balance display',
        'A': A_RESPONSES[0],
        'I': _NOT_EXECUTABLE,
        'L': f'{_NOT_EXECUTABLE} (incorrect parameter or balance with no display)'
    },
    'S': {
        'name': 'stable weight value',
        'S': 'current stable weight',
        'D': 'current non-stable weight',
        'I': _GENERAL_ERROR,
        'L': f'{_NOT_EXECUTABLE} (incorrect parameter)',
        '+': _OVERLOAD_MESSAGE,
        '-': _UNDERLOAD_MESSAGE,
    },
    'SI': {
        'name': 'weight value immediately',
        'S': 'stable weight value',
        'D': 'dynamic weight value',
        'I': _GENERAL_ERROR,
        '+': _OVERLOAD_MESSAGE,
        '-': _UNDERLOAD_MESSAGE,
    },
    # todo SIR
    # todo SIUM
    # todo complete class for SIX1
    'T': {  # todo catch and store tare memory value
        'name': 'tare the balance',
        'S': 'taring successfully performed',
        'I': _GENERAL_ERROR,
        '+': 'upper limit of taring range exceeded',
        '-': 'lower limit of taring range exceeded',
        'L': f'{_NOT_EXECUTABLE} (incorrect parameter)',
    },
    'TA': {
        'name': 'current tare value',
        'I': _GENERAL_ERROR,
        'L': f'{_NOT_EXECUTABLE} (incorrect parameter)',
    },
    'TAC': {
        'name': 'clear tare value',
        'I': _GENERAL_ERROR,
        'L': f'{_NOT_EXECUTABLE} (incorrect parameter)',
    },
    'TI': {
        'name': 'tare the balance immediately and independently of balance stability',
        'S': 'taring performed successfully with stable tare value',
        'D': 'taring performed successfully with dynamic tare value',
        'L': f'{_NOT_EXECUTABLE} (e.g. approved version of the balance)',
        '+': 'upper limit of taring range exceeded',
        '-': 'lower limit of taring range exceeded',
    },
    'Z': {
        'name': 'zero the balance',
        'A': 'zero setting performed',
        'I': 'zero setting not performed (balance is currently executing another command or stability was not reached)',
        '+': 'upper limit of zero setting range reached',
        '-': 'lower limit of zero setting range reached',
    },
    'ZI': {
        'name': 'zero the balance immediately and independently of balance stability',
        'D': 're-zero performed under non-stable (dynamic) conditions',
        'S': 're-zero performed under stable conditions',
        'I': 'zero setting not performed (balance is currently executing another command or stability was not reached)',
        '+': 'upper limit of zero setting range reached',
        '-': 'lower limit of zero setting range reached',
    },
    'M75': {
        'name': 'FACT protocol setting',
        'A': A_RESPONSES[0],
        'I': _GENERAL_ERROR,
        'L': f'{_NOT_EXECUTABLE} (incorrect parameter)',
    },
    # todo add ZERO commands
}


# todo check for system errors on connection (E01)
# todo check weighing mode
# todo consider implementing power and restart commands

# regex catch for a numeric value
_value_catch = '(\s(?P<value>.+))?'

_generic_command = re.compile(
    # some 3-character command which may be accompanied by a value
    f'(?P<prefix>[\w\d@]{{1,4}}){_value_catch}'
)


class BalanceCommand:
    # default value for command matching
    _base_regex = _generic_command

    def __init__(self,
                 command_string: str,
                 argument: str = None,
                 ):
        """
        A command for instructing or querying a MT balance. (Based on SICS command structure)

        :param command_string: command character(s)
        :param argument: argument for the command
        """
        self._prefix = None
        self._value = None
        self._match: re.Match = None
        self.command = command_string
        if self.value is None and argument is not None:
            self.value = argument
        # todo implement magic methods

    def __str__(self):
        return (
            f'{self.name}'
            f'{f" {self._value}" if self._value is not None else ""}'
        )

    def __repr__(self):
        return f'{self.__class__.__name__}({self.command_with_value})'

    @property
    def command(self) -> str:
        """command portion of the command string"""
        return self._prefix

    @command.setter
    def command(self, value: str):
        self._parse_command(value)

    def _parse_command(self, command_string: str):
        """
        Parses the command sets values accordingly

        :param command_string: command string to parse
        """
        match = self._base_regex.match(command_string.strip())
        if not match:
            raise ValueError(f'the provided command does not match a valid command syntax for {self.__class__.__name__}')
        self._match = match
        self._prefix = match.group('prefix')
        if 'value' in match.groupdict() and match.group('value') is not None:
            self.value = match.group('value')

    @property
    def command_with_value(self) -> str:
        """command with value appended (if any)"""
        return self.command + f'{f" {self._value}" if self._value is not None else ""}'

    def encode(self, encoding, errors):
        return self.command.encode(encoding, errors)

    @property
    def value(self):
        """value associated with the command"""
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    @property
    def dct(self) -> dict:
        """details of the command"""
        try:
            return SUPPORTED_COMMANDS[self.command]
        except KeyError:  # supports pass through of undefined commands
            return {}

    @property
    def name(self) -> str:
        """name/description of the command"""
        if 'name' not in self.dct:
            return 'UNDEFINED'
        return self.dct['name']

    def match(self, string: str):
        """
        Returns a regex match of the provided string with the instance's regex pattern

        :param string: string to match
        :return: matched object (re.Match)
        """
        return self._base_regex.match(string)


class BalanceResponse(BalanceCommand):
    # generic regex with explicit code between the command and value
    _base_regex = re.compile('(?P<prefix>[\w\d]{1,4})(\s(?P<code>.))?(\s+(?P<value>.+))?')

    def __init__(self,
                 response_string: str,
                 ):
        """
        Class for interpreting the response string for a command sent to a MT balance.

        :param response_string: string response received
        """
        self._response_code = None
        BalanceCommand.__init__(self, response_string)
        self.response_string = response_string

    def _parse_command(self, command_string: str):
        """
        Parses the command sets values accordingly

        :param command_string: command string to parse
        """
        super(BalanceResponse, self)._parse_command(command_string)
        if 'code' in self._match.groupdict():
            self._response_code = self._match.group('code')

    @property
    def error(self) -> str:
        """error description indicated in the response"""
        if self.error_code in self.dct:
            return self.dct[self.error_code]

    @property
    def error_code(self) -> str:
        """error code (if any)"""
        if self._response_code not in ['S', 'A', 'D']:
            return self._response_code

    @property
    def response_code(self) -> str:
        """response code returned from balance"""
        return self._response_code

    @property
    def response(self) -> str:
        """answer received from the balance"""
        if self.response_code in self.dct:
            return self.dct[self.response_code]

    @property
    def parsed_value(self) -> Union[str, UnitFloat]:
        """the value of the instance parsed"""
        try:
            return UnitFloat(self.value)
        except (ValueError, TypeError):
            return self.value

    def is_response_to_command(self, command: Union[str, BalanceCommand]) -> bool:
        """
        Checks whether the supplied command is a response to the provided command.

        :param command: command to check against
        :return: match
        """
        if isinstance(command, BalanceCommand) is False:
            command = BalanceCommand(command)
        return self.response_string.startswith(command.command)
