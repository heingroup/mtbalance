from setuptools import setup, find_packages

NAME = 'mtbalance'
AUTHOR = 'Lars Yunker / Hein Group'

PACKAGES = find_packages()
# KEYWORDS = ', '.join([
# ])

with open('LICENSE') as f:
    lic = f.read()
    lic.replace('\n', ' ')

# with open('README.MD') as f:
#     long_description = f.read()

setup(
    name=NAME,
    version='1.1.13',
    description='Python tools for controlling and communicating with a Mettler Toledo Quantos',
    # long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    url='https://gitlab.com/heingroup/mtbalance',
    packages=PACKAGES,
    license=lic,
    python_requires='>=3',
    classifiers=[
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'License :: OSI Approved :: MIT License',
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Operating System :: Microsoft :: Windows',
        'Natural Language :: English'
    ],
    # keywords=KEYWORDS,
    install_requires=[
        'unithandler>=1.3.1',
        'xmltodict',
        'pyserial',
    ],
    dependency_links=[
        'https://gitlab.com/heingroup/arduino_stepper_python/-/tree/master'
    ],
)
