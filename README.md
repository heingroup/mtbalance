# Quantos Python API wrapper

This repository contains a Python API wrapper for a Mettler Toledo Quantos solid dispenser. This code is in a 
development state and is a work in progress. Type hinting is implemented so Python >= 3.6 is required. Access to this code 
is restricted and is not for public dissemination without the express permission of Mettler Toledo and Jason Hein (UBC). 

Valid commands are mapped in `quantos.commands.COMMAND_MAP` and are abstracted into the `quantos.commands.QuantosCommand` 
class. Responses sent by the Quantos are automatically parsed in the `quantos.commands.QuantosResponse` class. 

The primary interaction class is the `quantos.api.Quantos` class, which has abstracted the commands into proerties and 
methods of the class in a human-readable form. Commands and responses are transferred over a network socket and responses 
are automatically retrieved with a dedicated thread. 

The XPE needs to be set up in a particular way for remote control. Use the touchscreen to set up the following settings:

![xpe_settings_1](doc/xpe_settings_1.jpg)

![xpe_settings_2](doc/xpe_settings_2.jpg)

![xpe_settings_3](doc/xpe_settings_3.jpg)

![xpe_settings_4](doc/xpe_settings_4.jpg)

![xpe_settings_5](doc/xpe_settings_5.jpg)

![xpe_settings_6](doc/xpe_settings_6.jpg)

![xpe_settings_7](doc/xpe_settings_7.jpg)

By default the XPE will do automatic internal readjustments. This will cause errors if you try to remotely control it while it is doing an internal adjustment; this can be disabled by:

![xpe_autoadjust_settings_1](doc/xpe_autoadjust_settings_1.jpg)

![xpe_autoadjust_settings_2](doc/xpe_autoadjust_settings_2.jpg)

![xpe_autoadjust_settings_3](doc/xpe_autoadjust_settings_3.jpg)